# Mesa performance tracking

## Introduction

This repository constains scripts to acquire data about the performance of Mesa drivers and also of Mesa's CI, and present it in Grafana.

It is composed of three parts:

- Scripts for the data acquisition and storage in InfluxDB
- Definition of Grafana dashboards in YAML
- A gitlab-ci pipeline for running these scripts preiodically and keeping the dashboards in Grafana up-to-date

After changes in production, the updated dashboards can be seen at:

- https://grafana.freedesktop.org/d/uINdWI3Mz/mesa-driver-performance
- https://grafana.freedesktop.org/d/-UCa0si7z/mesa-ci-stats

## Testing

A Docker compose definition is provided so contributors can test their changes locally before submitting their changes for review.

Steps:

1. Create a personal access token for your user with the API scope and set it as a masked variable of your fork with the name GITLAB_TOKEN
1. Disable shared runners in the user's fork repo
1. Build and run the containers containing Grafana, InfluxDB and gitlab-runner, specifying the runner registration token for your fork:
   ```$ RUNNER_REGISTRATION_TOKEN=[enter your token here] docker-compose up```
1. Check that you can log into the Grafana service by going to http://localhost:3000
1. Execute a pipeline in your fork
1. After the pipeline finishes, test your changes in http://localhost:3000

